import React from "react";
import { Image } from "react-native";
import { Header, Left, Body, Right, Button, Icon, Title } from "native-base";
import gojekLogo from "../../../assets/icon/gojek-logo_normal.svg";
import { SvgUri } from "react-native-svg";
import color from "../../utils/color";

export default AppHeader = () => {
  return (
    <Header style={{ backgroundColor: "white" }}>
      <Left style={{ marginLeft: 20 }}>
        <SvgUri
          width={100}
          height={30}
          uri={
            "https://lelogama.go-jek.com/service_icon_text/gojek-logo_normal.svg"
          }
        />
      </Left>
      <Right>
        <Button transparent>
          <Icon
            type="FontAwesome"
            name="bars"
            style={{ color: color.primary }}
          />
        </Button>
      </Right>
    </Header>
  );
};
