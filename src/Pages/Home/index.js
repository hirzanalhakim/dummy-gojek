import React from "react";
import { StyleSheet, Text, View, Image, Dimensions } from "react-native";
import { Container, Content, Icon } from "native-base";
import {
  AppHeader,
  CarouselHeader,
  CarouselContent,
  CarouselCustom,
} from "../../Components";
import color from "../../utils/color";
import {
  dataCarouselHeader,
  dataCarouselContent,
  dataTransportLogistic,
  dataFood,
  dataPayments,
  dataNews,
  dataBusiness,
} from "../../utils/mockData";

const WIDTH = Dimensions.get("window").width;

export default function Home() {
  return (
    <Container>
      <AppHeader />
      <Content>
        <CarouselHeader data={dataCarouselHeader} />
        <View style={styles.content}>
          <Text style={styles.titleContent}>
            About Yayasan Anak Bangsa Bisa
          </Text>
          <Image
            source={{
              uri:
                "https://lelogama.go-jek.com/component/information/Mobile-Ver_1.jpg",
            }}
            resizeMode="contain"
            style={styles.imageYayasan}
          />
          <Text style={styles.textContent}>
            Yayasan Anak Bangsa Bisa (YABB) is a non-profit organization founded
            by Gojek to advance equal opportunities and help build more
            sustainable livelihoods for societies who rely on daily income
          </Text>
          <Text
            style={{ color: color.primary, fontSize: 20, fontWeight: "bold" }}
          >
            Find Out Here{" "}
            <Icon
              name="arrowright"
              type="AntDesign"
              style={{ color: color.primary, fontSize: 16 }}
            />
          </Text>
        </View>

        <CarouselContent data={dataCarouselContent} />

        {/* SERVICES */}
        <View style={styles.content}>
          <Text style={styles.titleContent}>Our Services</Text>
          <Text style={styles.subTitleContent}>Transport & Logistic</Text>
          <Text style={styles.textContent}>
            Moving something (or someone) from point A to point B? Our Gojek
            drivers in green jackets and helmets save your time, and energy.
          </Text>
        </View>
        <CarouselCustom data={dataTransportLogistic} />

        <View style={[styles.content, { marginTop: 20 }]}>
          <Text style={styles.subTitleContent}>Food & FMCG</Text>
          <Text style={styles.textContent}>
            Order food from a whole lotta restaurants, get medicines delivered
            in a jiffy, or fill your shopping cart from many a mart.
          </Text>
        </View>
        <CarouselCustom data={dataFood} />

        <View style={[styles.content, { marginTop: 20 }]}>
          <Text style={styles.subTitleContent}>Payments</Text>
          <Text style={styles.textContent}>
            From split the bills to donations, we make payments reliable, easy
            and delightful for our customers and merchants alike.
          </Text>
        </View>
        <CarouselCustom data={dataPayments} />

        <View style={[styles.content, { marginTop: 20 }]}>
          <Text style={styles.subTitleContent}>News & Entertainment</Text>
          <Text style={styles.textContent}>
            Binge on your favourite series, book tickets to the next concert,
            organise celebrity events, and do much more. All your treasured
            digital content belong here.
          </Text>
        </View>
        <CarouselCustom data={dataNews} />

        <View style={[styles.content, { marginTop: 20 }]}>
          <Text style={styles.subTitleContent}>Business</Text>
          <Text style={styles.textContent}>Your business partner.</Text>
        </View>
        <CarouselCustom data={dataBusiness} />
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  imageYayasan: {
    width: WIDTH - 36,
    height: 240,
    borderRadius: 15,
    marginBottom: 20,
  },
  content: {
    paddingHorizontal: 18,
    marginBottom: 20,
  },
  titleContent: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  subTitleContent: {
    fontSize: 22,
    fontWeight: "bold",
    marginBottom: 15,
  },
  textContent: {
    fontSize: 16,
    lineHeight: 25,
    marginBottom: 16,
  },
});
