import React from "react";
import { View, Text, StyleSheet, Dimensions, Image } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import color from "../../utils/color";

export const WIDTH = Dimensions.get("window").width;

const CarouselCardHeader = ({ item, index }) => {
  return (
    <View style={styles.container} key={index}>
      <Image source={{ uri: item.imgUrl }} style={styles.image} />
    </View>
  );
};

const CarouselHeader = ({ data }) => {
  const [index, setIndex] = React.useState(0);
  const isCarousel = React.useRef(null);
  return (
    <>
      <Carousel
        layout="stack"
        layoutCardOffset={9}
        ref={isCarousel}
        data={data}
        renderItem={CarouselCardHeader}
        sliderWidth={WIDTH}
        itemWidth={WIDTH}
        onSnapToItem={(index) => setIndex(index)}
      />
      <Pagination
        dotsLength={data.length}
        activeDotIndex={index}
        carouselRef={isCarousel}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 0,
          marginTop: 0,
        }}
        dotColor={color.primary}
        inactiveDotColor="#d3d3d3"
        inactiveDotScale={0.6}
        tappableDots={true}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    borderRadius: 8,
    width: WIDTH,
    backgroundColor: "red",
    // paddingBottom: 40,
  },
  image: {
    width: WIDTH,
    height: 500,
  },
});

export default CarouselHeader;
