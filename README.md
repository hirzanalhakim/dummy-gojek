This project was created with [EXPO CLI](https://docs.expo.io/get-started/installation/)

## How to run this project :

1. Clone this project
2. Go to the project's folder that has been Clone
3. yarn install / npm install (if you don't have expo cli installed at your laptop/macbook, please [install](https://docs.expo.io/get-started/installation/) first )
4. yarn ios (for running in ios) || yarn android (for running in android)

