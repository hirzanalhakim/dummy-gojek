import React from "react";
import { View, Text, StyleSheet, Dimensions, Image } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import color from "../../utils/color";

export const WIDTH = Dimensions.get("window").width;

const CarouselCardContent = ({ item, index }) => {
  return (
    <View style={styles.container} key={index}>
      <Image source={{ uri: item.imgUrl }} style={styles.image} />
    </View>
  );
};

const CarouselContent = ({ data }) => {
  const [index, setIndex] = React.useState(0);
  const isCarousel = React.useRef(null);
  return (
    <>
      <Carousel
        layout="stack"
        layoutCardOffset={15}
        ref={isCarousel}
        data={data}
        renderItem={CarouselCardContent}
        sliderWidth={WIDTH}
        itemWidth={WIDTH - 40}
        onSnapToItem={(index) => setIndex(index)}
      />
      <Pagination
        dotsLength={data.length}
        activeDotIndex={index}
        carouselRef={isCarousel}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 0,
          marginTop: 0,
        }}
        dotColor={color.primary}
        inactiveDotColor="#d3d3d3"
        inactiveDotScale={0.6}
        tappableDots={true}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    width: WIDTH,
  },
  image: {
    width: WIDTH - 40,
    height: 220,
    borderRadius: 20,
  },
});

export default CarouselContent;
