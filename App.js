import React, { useState, useEffect } from "react";
import * as Font from "expo-font";
import { Ionicons } from "@expo/vector-icons";
import AppLoading from "expo-app-loading";
import { Home } from "./src/Pages";

export default function App() {
  const [isReady, setReady] = useState(false);

  useEffect(() => {
    native_base();
    setReady(true);
  });

  const app = !isReady ? <AppLoading /> : <Home />;
  return app;
}

async function native_base() {
  await Font.loadAsync({
    Roboto: require("native-base/Fonts/Roboto.ttf"),
    Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
    ...Ionicons.font,
  });
}
