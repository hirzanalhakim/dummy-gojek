export const dataCarouselHeader = [
  {
    title: "Aenean leo",
    body:
      "Ut tincidunt tincidunt erat. Sed cursus turpis vitae tortor. Quisque malesuada placerat nisl. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.",
    imgUrl:
      "https://lelogama.go-jek.com/component/hero/picture_mobile/Mobile_KV_User_720x960.jpg",
  },
  {
    title: "In turpis",
    body:
      "Aenean ut eros et nisl sagittis vestibulum. Donec posuere vulputate arcu. Proin faucibus arcu quis ante. Curabitur at lacus ac velit ornare lobortis. ",
    imgUrl:
      "https://lelogama.go-jek.com/component/hero/picture_mobile/FA---Brand---Mobile-Web-Banner---720px-x-960px-.jpg",
  },
  {
    title: "Lorem Ipsum",
    body:
      "Phasellus ullamcorper ipsum rutrum nunc. Nullam quis ante. Etiam ultricies nisi vel augue. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc.",
    imgUrl:
      "https://lelogama.go-jek.com/component/hero/picture_mobile/J3K_mobile_Web-01.jpg",
  },
];

export const dataCarouselContent = [
  {
    imgUrl:
      "https://lelogama.go-jek.com/component/banner/picture_mobile/Help_Banner-Mobile_EN.jpg",
  },
  {
    imgUrl:
      "https://lelogama.go-jek.com/component/banner/picture_mobile/english_go-ride2x_mobile_2.jpg",
  },
  {
    imgUrl:
      "https://lelogama.go-jek.com/component/banner/picture_mobile/English_gocar_mobile.jpg",
  },
  {
    imgUrl:
      "https://lelogama.go-jek.com/component/banner/picture_mobile/Go-Food_English2x_mobile_2.jpg",
  },
  {
    imgUrl:
      "https://lelogama.go-jek.com/component/banner/picture_mobile/Gopay_English2x_mobile2.jpg",
  },
];

export const dataTransportLogistic = [
  [
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/goride_vertical.svg",
    },
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/gocar_vertical.svg",
    },
  ],
  [
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/bluebird_vertical.svg",
    },
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/gosend_vertical.svg",
    },
  ],
  [
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/gobox_vertical.svg",
    },
  ],
];

export const dataFood = [
  [
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/gofood_vertical.svg",
    },
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/gomed_vertical.svg",
    },
  ],
  [
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/gomart_vertical.svg",
    },
  ],
];

export const dataPayments = [
  [
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/gopay_vertical.svg",
    },
    {
      imgUrl: "https://lelogama.go-jek.com/service_icon_text/gotagihan.svg",
    },
  ],
  [
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/vertical_paylater_1.svg",
    },
    {
      imgUrl: "https://lelogama.go-jek.com/service_icon_text/gosure.svg",
    },
  ],
  [
    {
      imgUrl: "https://lelogama.go-jek.com/service_icon_text/gosure.svg",
    },
  ],
];

export const dataNews = [
  [
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/goplay_vertical.svg",
    },
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/gotix_vertical.svg",
    },
  ],
];
export const dataBusiness = [
  [
    {
      imgUrl:
        "https://lelogama.go-jek.com/service_icon_text/gobiz_vertical.svg",
    },
  ],
];
