import React from "react";
import { View, Text, StyleSheet, Dimensions, Image } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { SvgUri } from "react-native-svg";
import color from "../../utils/color";

export const WIDTH = Dimensions.get("window").width;

const CarouselCardCustom = ({ item, index }) => {
  return (
    <View
      style={{ flexDirection: "row", justifyContent: "space-between" }}
      key={index}
    >
      {item &&
        item.map((display, i) => {
          return (
            <View style={styles.container} key={i}>
              <SvgUri width="100%" height="100%" uri={display.imgUrl} />
            </View>
          );
        })}
    </View>
  );
};

const CarouselCustom = ({ data }) => {
  const [index, setIndex] = React.useState(0);
  const isCarousel = React.useRef(null);
  return (
    <>
      <Carousel
        layout="default"
        // layoutCardOffset={150}
        ref={isCarousel}
        data={data}
        renderItem={CarouselCardCustom}
        sliderWidth={WIDTH}
        itemWidth={WIDTH - 40}
        onSnapToItem={(index) => setIndex(index)}
      />
      <Pagination
        dotsLength={data.length}
        activeDotIndex={index}
        carouselRef={isCarousel}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 0,
          marginTop: 0,
        }}
        dotColor={color.primary}
        inactiveDotColor="#d3d3d3"
        inactiveDotScale={0.6}
        tappableDots={true}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    width: "48%",
    backgroundColor: "white",
    height: 140,
    borderWidth: 1,
    borderColor: "#d3d3d3",
  },
});

export default CarouselCustom;
